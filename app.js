/*-----------------------------------------------------------------------------
 * @package:    REST Server Project JavaScript Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/

// GLOBALS

/* Google Vars
const PROJECT_ID = "YOUR_PROJECT_ID";
const SERVICE_ID = "YOUR_SERVICE_ID";
const VERSION = "0.1.0;"
const TRACE_KEY_FILE = "~/.local/share/google-cloud/agents/service-accounts/keys/key.json";

// Google Trace Agent
require( '@google-cloud/trace-agent' ).start
(
    {
        projectId: PROJECT_ID,
        keyFilename: TRACE_KEY_FILE
    }
);

// Google Profiler
require( '@google-cloud/profiler' ).start
(
    {
        projectId: PROJECT_ID,
        serviceContext: {
            service: SERVICE_ID,
            version: VERSION
        },
    }
);*/

delete require.cache['@kwaeri/session'];


// DEFINES:
let env      = ( ( process.env.NODE_ENV && process.env.NODE_ENV !== ( "" || null ) ) ?
                    process.env.NODE_ENV : "default" ),
    config   = require( `./conf/app.${env}.json` ),
    session  = require( `./conf/sessions.${env}.json` ),    // Session configuration uses `locations` instead of `paths` when utilizing memcached
                                                            // as either a string (for 1 server) or an array of strings (for numerous servers)
    store    = require( '@kwaeri/session/build/src/memcached-session-store' ).MemcachedSessionStore,
    //store  = require( '@kwaeri/session/build/src/filesystem-session-store' ).FilesystemSessionStore,
    db       = require( `./conf/database.${env}.json` ),
    //connector= require( '@kwaeri/mysql-database-driver' ),
    nodekit  = require( '@kwaeri/node-kit' ).nodekit;

let nk = new nodekit( { ...config, session: session, store: store, database: db, /*connector: connector*/ } );


// Start the application:
nk.listen();

