/*-----------------------------------------------------------------------------
 * @package:    REST Server Project JavaScript Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import babel from 'gulp-babel';
import sourcemaps from 'gulp-sourcemaps';
import rename from 'gulp-rename';                   // For adding suffixes/renaming files (we use it to name minified js files)
import gts from 'gulp-typescript';                  // For compiling typescript, requires the typescript module also.
import watch from 'gulp-watch';                     // For Auto-Compilation/Transpilation upon save
import del from 'del';                              // Used by our clean process, for the most part -
//import bump from 'gulp-bump-version';               // Used for version increment automation


// DEFINES
let gtsProject  = gts.createProject( 'tsconfig.json' ); // A preliminary for transpiling typescript using microsoft's compiler


// PARSE COMMAND LINE ARGUMENTS:
const args =
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^-+/, '' );

            if( option === thisOption )
            {
                // Argument value:
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check if we supplied arguments specific to version bump processes:
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly:
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = { version: toVersion, key: '"version": "' };
}

// If type was provided, overwrite the options accordingly:
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = { type: byType, key: '"version": "' };
}


// Bumps the version in our projects files (called manually, or through the build-release task):
gulp.task
(
    'bump-file-versions',
    () =>
    {
        // Otherwise, by providing --bump-version only, you will increment the patch revision

        // Start with all the files using the typical key, then hit our package.json: 'build/node-kit/**/*.js'
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.js',
                '**/*.scss',
                '!node_modules{,/**}',
                '!test{,/**}',
                '!build/test{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bumps the version in our package.json file:
gulp.task
(
    'bump-project-version',
    () =>
    {
        // Target our package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Compiles Typescript files for development:
gulp.task
(
    'compile-typescript-development',
    () =>
    {
        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( sourcemaps.init() )
        .pipe( gtsProject() )
        .js.pipe( sourcemaps.write( '.' ) )
        .on( error, ( error ) => console.log( error ) )
        .pipe( gulp.dest( 'app' ) );
    }
);




// Compiles Typescript files for production:
gulp.task
(
    'compile-typescript',
    () =>
    {
        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .js.pipe( gulp.dest( 'app' ) );
    }
);


// Generates Typescript declaration files:
gulp.task
(
    'generate-typescript-declarations',
    () =>
    {
        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .dts.pipe( gulp.dest( 'build' ) );
    }
);


gulp.task
(
    'transpile-es6:development',
    () =>
    {
        return gulp.src
        (
            [
                'node_modules/@babel/polyfill/dist/polyfill.js',
                'src/**/*.js'
            ]
        )
        .pipe( sourcemaps.init() )
        .pipe( babel( { presets: ['@babel/preset-env'] } ) )
        .pipe( sourcemaps.write( '.' ) )
        .on( error, ( error ) => console.log( error ) )
        .pipe( gulp.dest( 'app' ) )
    }
);


gulp.task
(
    'transpile-es6',
    () =>
    {
        return gulp.src
        (
            [
                'node_modules/@babel/polyfill/dist/polyfill.js',
                'src/**/*.js'
            ]
        )
        .pipe( babel( { presets: ['@babel/preset-env'] } ) )
        .on( error, ( error ) => console.log( error ) )
        .pipe( gulp.dest( 'app' ) )
    }
);


// Moves controllers/models to app/, and removes the test directory:
gulp.task
(
    'reconcile-artifacts',
    () =>
    {
        return gulp.src( [ 'app/src/**/*' ] )
        .pipe( gulp.dest( 'app' ) );
    }
);


// Watches for changes in TS files:
gulp.task
(
    'watch:typescript',
    () =>
    {
        gulp.watch( '**/*.ts', ['default'] );
    }
);


// Watches for changes in ES6 JS files:
gulp.task
(
    'watch:javascript',
    () =>
    {
        gulp.watch( '**/*.js', ['default'] );
    }
);


// Cleans the build directory (removes it):
gulp.task
(
    'clean:app',
    () =>
    {
        let deleteList = ( !args.alt ) ? ['app/src', 'app/tests'] : ['app/src', 'app/tests'];
        del.sync
        (
            deleteList,
            { force: true }
        );

        // While typescript compiler can just return its process,
        // for some reason del.sync() needs to trigger async
        // completion (ensure you provide the argument to the
        // anon function so that it's available):
        done();    }
);


// Cleans the build directory (removes it):
gulp.task
(
    'clean:build',
    () =>
    {
        let deleteList = ( !args.alt ) ? 'app' : 'app';
        del.sync
        (
            deleteList,
            { force: true }
        );

        // While typescript compiler can just return its process,
        // for some reason del.sync() needs to trigger async
        // completion (ensure you provide the argument to the
        // anon function so that it's available):
        done();    }
);


// Runs clean processes:
gulp.task
(
    'clean',
    gulp.series
    (
        'clean:build',
        ( done ) =>
        {
            done();
        }
    )
);


// Cleans the dist directory and bump all file header version strings:
gulp.task
(
    'prepare-release',
    gulp.series
    (
        'clean:build',
        'bump-project-version',
        'bump-file-versions',
        ( done ) =>
        {
            done();
        }
    )
);


// Cleans the dist directory and bump all file header version strings:
gulp.task
(
    'build-development',
    gulp.series
    (
        'clean:build',
        'transpile-es6-development',
        'reconcile-artifacts',
        'clean:app',
        ( done ) =>
        {
            done();
        }
    )
);


// Runs default build process:
gulp.task
(
    'default',
    gulp.series
    (
        'clean',
        'transpile-es6',
        'reconcile-artifacts',
        'clean:app',
        ( done ) =>
        {
            done();
        }
    )
);

