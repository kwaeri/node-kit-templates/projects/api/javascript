# REST Server Project JavaScript Template

[![pipeline status](https://gitlab.com/kwaeri/node-kit-templates/projects/api/javascript/badges/master/pipeline.svg)](https://gitlab.com/kwaeri/node-kit-templates/projects/api/javascript/commits/master)  [![coverage report](https://gitlab.com/kwaeri/node-kit-templates/projects/api/javascript/badges/master/coverage.svg)](https://kwaeri.gitlab.io/node-kit-templates/projects/api/javascript/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

REST server project template (in javascript).

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

This template makes use of current standards and best practices. The `src` directory contains a rather straight-forward hierarchy:

* `controllers`
* `models`

The `controllers` directory is where action controllers will live, and are automatically generated there when using - for instance - [kue](https://gitlab.com/kwaeri/node-kit/cli) (`kue add endpoint MyEndpoint`) to cater development. Each controller live either within a named directory (for granular organization) complete with:

* An index roll-up
* A named action file

Or within a named controller file. This convention offers developers the freedom to organize their business logic as it meets their needs - and is a natural side-effect of the platform.

The `models` directory is where models will live, and are also automatically generated there when using - for instance - [kue](https://gitlab.com/kwaeri/node-kit/cli) (`kue add endpoint --with-model MyEndpointAndModel`) to cater development. Models have the same freedom to organization that controllers possess.

The `conf` directory is where you&amp;ll find configuration files for just about every aspect of the REST server that needs configuring. You'll want to peruse those files and make changes as needed. What's present in the configuration files is same for a simple test environment assuming you configure your test environment accordingly.

## Getting Started

You can take and use the base template, or generate a project from a more complete template that includes an innovative world-class user-management implementation. Discover all the different things you can do with [@kwaeri/node-kit](https://gitlab.com/kwaeri/node-kit/wiki).

### Installation

To install the project dependencies, run the following command(s):

```bash
npm install .
```

### Usage

To add a controller to the project, leverage the [cli](https://gitlab.com/kwaeri/node-kit/cli):

```bash
kue add endpoint MyNewController
```

You can choose to add a model as well with `--with-model`. A command to add just a model hasn't been worked in yet, but you should expect one will exist before long.

To run the project:

```bash
npm start
```

To debug the project:

```bash
npm debug
```

**NOTE**

These templates are currently under construction. They *should* work, but as we finish with revamping the @kwaeri/node-kit platform and framework - we'll address any issues that come up.

More documentation to come!

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-node-kit/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:contact-project+kwaeri-node-kit-templates-projects-api-javascript-37335298-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-node-kit/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

